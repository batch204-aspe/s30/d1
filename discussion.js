db.course_bookings.insertMany([
		 {
            "courseId": "C001",
            "studentId": "S004",
            "isCompleted": true
        },
        {
            "courseId": "C002",
            "studentId": "S001",
            "isCompleted": false
        },
        {
            "courseId": "C001",
            "studentId": "S003",
            "isCompleted": true
        },
        {
            "courseId": "C003",
            "studentId": "S002",
            "isCompleted": false
        },
        {
            "courseId": "C001",
            "studentId": "S002",
            "isCompleted": true
        },
        {
            "courseId": "C004",
            "studentId": "S003",
            "isCompleted": false
        },
        {
            "courseId": "C002",
            "studentId": "S004",
            "isCompleted": true
        },
        {
            "courseId": "C003",
            "studentId": "S007",
            "isCompleted": false
        },
        {
            "courseId": "C001",
            "studentId": "S005",
            "isCompleted": true
        },
        {
            "courseId": "C004",
            "studentId": "S008",
            "isCompleted": false
        },
        {
            "courseId": "C001",
            "studentId": "S013",
            "isCompleted": true
        }
	]);

/*
	Aggregation PipeLine Syntax:

		db.collectionName.aggregate([
			{Stage 1},
			{Stage 2},
			{Stage 3}
		]);

*/

/*
	$group is used to group elements/documents together & create an analysis of these grouped documents

	Syntax:
		{
			$group: {_id: <id>, fieldResult: "valueResult"}
		}

*/

// Count all the documents in our database
db.course_bookings.aggregate([
		{
			$group: {
				_id: null, count: {$sum: 1}
			}
		}
	]);

db.course_bookings.aggregate([
		{
			$group: {
				_id: "$studentId", count: {$sum: 1}
			}
		}
	]);

/*
	$match is used to pass the documnets or get the documents that will match our condition

	SYntax:
		{
			$match: {field: value}
		}

*/

db.course_bookings.aggregate([
		{
			$match: {
				"isCompleted": true
			}
		},

		{
			$group: {
				_id: "$courseId", total: {$sum: 1}
			}
		}
	]);

/*
	$project - this will alows us to show or hide the details

	SYntax:
		{
			$project: { field: 0 or 1 } // 0 - is to hide & 1 - is to show
		}
	
*/

db.course_bookings.aggregate([
		{
			$match: {
				"isCompleted": true
			}
		},

		{
			$project: {
				 "courseId": 0,
				 "studentId": 0
			}
		}
	]);

// used to count or to get the total sum of the documents
db.course_bookings.count();

/*
	$sort - can be used to changed the order of the aggregated results

	Syntax:
		{
			$sort: { field: 1/-1 } // 1 - is for asc order & -1 - is for desc order
		}
*/


db.course_bookings.aggregate([
		{
			$match: {
				"isCompleted": true
			}
		},

		{
			$sort: {
				"courseId": -1
			}
		}
	]);

/*
	Mini Activity:
		1. Count the completed courses of student s013
		
		2. Sort the courseId in desc order while student ID in ascending order

*/

// Solution 1
db.course_bookings.aggregate([
		{
			$match: {
				"studentId": "S013",
				"isCompleted": true
			}
		},

		{
			$group: {
				_id: "$studentId", count: {$sum: 1}
			}
		}
	]);

//Other Solution
db.course_bookings.aggregate([
		{
			$match: {
				"studentId": "S013",
				"isCompleted": true
			}
		},

		{
			$count: "totalNumberofCompletedCourse"
		}
	]);

// Solution 2
db.course_bookings.aggregate([

		{
			$sort: {
				"courseId": -1,
				"studentId": 1
			}
		}

	]);

/*********************************/ 
db.orders.insertMany([
		{
			"cust_Id": "A123",
			"amount": 500,
			"status": "A"
		},

		{
			"cust_Id": "A123",
			"amount": 250,
			"status": "A"
		},

		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "A"
		},

		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "D"
		},
	]);

db.orders.aggregate([
		{
			$match: {
				"status": "A"
			}
		},

		{
			$group: {
				_id: "$cust_Id", total: {$sum: "$amount"}
			}
		}

	]);

/*
	$sum - getting the total number
	$avg - getting the average
	$min - getting the min value
	$max - getting the max value

*/

db.orders.aggregate ([
		{
			$match: {
				"status" : {
					$in: ["A", "D"]
				}
			}
		},

		{
			$group: {
				_id: "$cust_Id", maxAmount: {$max: "$amount"}
			}
		}
	]);